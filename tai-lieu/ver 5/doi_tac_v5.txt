{strip}
{if !empty($data_block)}
	{if !empty($data_extend['locale'][{LANGUAGE}]['tieu_de'])}
        <h3 class="title-section text-center ">
            {$this->Block->getLocale('tieu_de', $data_extend)}
        </h3>
    {/if}

	<div class="slider-brand">
		<div nh-owl-slick="{if !empty($data_extend.slider)}{htmlentities($data_extend.slider|@json_encode)}{/if}">
			{foreach from = $data_block item = slider key = k_slider name = each_slider}			
				{assign var = image_source value = ''}
				{if !empty($slider.image) && !empty($slider.image_source)}
					{assign var = image_source value = $slider.image_source}
				{/if}

				{assign var = image_url value = ''}
				{if !empty($slider.image) && $image_source == 'cdn'}
					{assign var = image_url value = "{CDN_URL}{$slider.image}"}
				{/if}

				{if !empty($slider.image) && $image_source == 'template'}
					{assign var = image_url value = "{$slider.image}"}
				{/if}
				
				{assign var = url value = '/'}
				{if !empty($slider.url)}
					{assign var = url value = $this->Utilities->checkInternalUrl($slider.url)}
				{/if}

				<div class="item {if !empty($slider.class_item)}{$slider.class_item}{/if}">
					<div class="item-brand border position-relative">
                        {$this->LazyLoad->renderImage([
                            'src' => $image_url, 
                            'alt' => "{if !empty($slider.name)}{$slider.name}{/if}", 
                            'class' => 'img-fluid rti-abs-cover rti-abs-contain'
                        ])}
					</div>
				</div>
			{/foreach}
		</div>
	</div>
{/if}
{/strip}

{
	"locale": {
		"vi": {
			"tieu_de": "Đối tác"
		}
	},
	"slider": {
		"infinite": true,
		"slidesToShow": 5,
		"slidesToScroll": 1,
		"dots": false,
		"arrows": true,
		"adaptiveHeight": true,
		"autoplay": false,
		"autoplaySpeed": 2000,
		"responsive": [
			{
				"breakpoint": 1024,
				"settings": {
					"infinite": true,
					"dots": true
				}
			},
			{
				"breakpoint": 600,
				"settings": {
					"slidesToShow": 4,
					"slidesToScroll": 1
				}
			},
			{
				"breakpoint": 480,
				"settings": {
					"slidesToShow": 2,
					"slidesToScroll": 1
				}
			}
		]
	},
	"col": "col-lg-3 col-md-6 col-6"
}

{strip}
{if !empty($data_block)}
    <div class="section-brand">
        {if !empty($data_extend['locale'][{LANGUAGE}]['tieu_de'])}
            <h3 class="title-section text-center">
                {$this->Block->getLocale('tieu_de', $data_extend)}
            </h3>
        {/if}
    	<div class="swiper swiper-slider-brand" nh-swiper="{if !empty($data_extend.slider)}{htmlentities($data_extend.slider|@json_encode)}{/if}">
    	    <div class="swiper-wrapper">
    	    	{foreach from = $data_block item = slider}
    	    		{assign var = image_source value = ''}
    				{if !empty($slider.image) && !empty($slider.image_source)}
    					{assign var = image_source value = $slider.image_source}
    				{/if}
    
    				{assign var = image_url value = ''}
    				{if !empty($slider.image) && $image_source == 'cdn'}
    					{assign var = image_url value = "{CDN_URL}{$slider.image}"}
    					{if !empty(DEVICE)}
    					    {assign var = image_url value = "{CDN_URL}{$this->Utilities->getThumbs($slider.image, 350)}"}
    					{/if}
    				{/if}
    
    				{if !empty($slider.image) && $image_source == 'template'}
    					{assign var = image_url value = "{$slider.image}"}
    					{if !empty(DEVICE)}
    					    {assign var = image_url value = "{$this->Utilities->getThumbs($slider.image, 350, 'template')}"}
    					{/if}
    				{/if}
    				
    		        <div class="swiper-slide {if !empty($slider.class_item)}{$slider.class_item}{/if}">
		                <div class="item-img-brand">
		                    <div class="img-brand ratio-16-9">
		                        <img src="{$image_url}" class="img-fluid" alt="{if !empty($slider.name)}{$slider.name}{/if}">
		                    </div>
		                </div>
    		        </div>
    	        {/foreach}
    	    </div>
    	    {if !empty($data_extend.slider.pagination)}
    		    <!-- If we need pagination -->
    		    <div class="swiper-pagination"></div>
    	    {/if}
    	</div>
    	{if !empty($data_extend.slider.navigation)}
		    <!-- If we need navigation buttons -->
		    <div class="swiper-button-next-2">
	            <i class="fa-light fa-angle-right display-2"></i>
	        </div>
	        <div class="swiper-button-prev-2">
	            <i class="fa-light fa-angle-left display-2"></i>
        </div>
        {/if}
    </div>
{/if}

{/strip}

{
	"slider": {
		"slidesPerView": 2,
		"spaceBetween": 10,
		"grid": {
			"rows": 2
		},
		"autoplay": {
			"delay": 4000,
			"disableOnInteraction": true,
			"pauseOnMouseEnter": true
		},
		"pagination": {
			"enabled": false,
			"el": ".swiper-pagination",
			"clickable": true
		},
		"navigation": {
			"nextEl": ".swiper-button-next-2",
			"prevEl": ".swiper-button-prev-2"
		},
		"breakpoints": {
			"640": {
				"slidesPerView": 3,
				"spaceBetween": 10
			},
			"768": {
				"slidesPerView": 4,
				"spaceBetween": 20
			},
			"1024": {
				"slidesPerView": 5,
				"spaceBetween": 20
			}
		}
	}
}