<?php
declare(strict_types=1);

namespace App\View\Helper;
use Cake\View\Helper;
use Cake\Core\Exception\Exception;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\View\Helper\UrlHelper;
use Cake\Routing\Router;

class UtilitiesHelper extends Helper
{
    public function convertIntgerToDateString($int = null, $format = 'd/m/Y')
    {
        if(empty($int)) return null;
        if(empty($format)) $format = 'd/m/Y';

        try{
            $result = date(strval($format), intval($int));
        }catch (Exception $e) {
            return null;
        }
        return $result;
    }

    public function convertIntgerToDateTimeString($int = null, $format = 'H:i - d/m/Y')
    {
        if(empty($int)) return null;
        if(empty($format)) $format = 'H:i - d/m/Y';

        try{
            $result = date(strval($format), intval($int));
        }catch (Exception $e) {
            return null;
        }
        
        return $result;
    }

    public function formatDateTimeClient($str_time = null, $format = 'H:i - d/m/Y')
    {
        if(empty($str_time)) return null;

        $int = $this->stringDateTimeClientToInt($str_time);

        if(empty($format)) $str_time;

        try{
            $result = date(strval($format), intval($int));
        }catch (Exception $e) {
            return null;
        }
        
        return $result;
    }

    public function stringDateTimeClientToInt($str_date = null)
    {
        // check datetime H:i - d/m/Y 
        if(!$this->isDateTimeClient($str_date)){
            return null;
        }

        $time = Time::createFromFormat('H:i - d/m/Y', $str_date, null);
        $time = $time->format('Y-m-d H:i:s');
        return strtotime($time);
    }

    public function isDateTimeClient($str = null)
    {
        // check datetime H:i - d/m/Y
        $matches = [];
        $pattern = '/^([0-9]{1,2})\:([0-9]{1,2})\s\-\s([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
        if (!preg_match($pattern, $str, $matches)) return false;

        return true;
    }

    public function getCurrentDate($format = null)
    {
        if(empty($format)) $format = 'Y-m-d';

        try{
            $date = date(strval($format));
        }catch (Exception $e) {
            return null;
        }

        return $date;
    }

    public function randomCode($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyz', intval(ceil($length/strlen($x))))), 1, $length);
    }

    public function getThumbs($url = null, $size = null)
    {
        if(empty($url) || empty($size) || !in_array($size, [50, 150, 250, 350])) return null;

        $path = explode('/', $url);
        $path[1] = 'thumbs';
        $path_info = pathinfo($url);
        $extension = !empty($path_info['extension']) ? $path_info['extension'] : '';
        
        if(empty($extension)) return null;
        
        $filename = !empty($path_info['filename']) ? $path_info['filename'] : '';
        $num_last =  count($path) - 1;
        $path[$num_last] = $filename . '_thumb_'. $size . '.'. $extension;

        return implode('/', $path);
    }

    public function getFileNameInUrl($url_file = null)
    {
        if(empty($url_file)) return null;
        return pathinfo($url_file, PATHINFO_BASENAME);
    }    

    public function checkInternalUrl($url = null)
    {
        if(empty($url)) return '';

        if(strpos($url, $this->getUrlWebsite()) === 0){
            $url = str_replace($this->getUrlWebsite(), '', $url);
        }

        if(strpos($url, 'https://') === 0 || strpos($url, 'http://') === 0 || strpos($url, 'www') === 0 || strpos($url, 'https://www') === 0){
            return $url;
        }

        if(strpos($url, '/') === 0){
            return $url;
        }
        
        return '/' . $url;
    }

    public function addParamsToUrl($url = null, $add = [], $remove = [], $options = [])
    {
        if(empty($url)){
            $url = '/';
        }

        if(empty($add) && empty($remove)) return $url;

        $url_data = parse_url($url);
        $path = !empty($url_data['path']) ? $url_data['path'] : '';
        $query = [];
        if(!empty($url_data['query'])){
            $tmp = [];
            parse_str($url_data['query'], $tmp);
            
            foreach ($tmp as $k => $value) {
                $k = str_replace('amp;', '', $k);
                $query[$k] = $value;
            }
        }
        if(!empty($add)){
            foreach ($add as $key => $value) {
                if(!empty($options['merge']) && !empty($query[$key])){
                    $value = $query[$key] . '-' . $value;
                    $list_value = array_unique(explode('-', $value));
                    $query[$key] = implode('-', $list_value);
                }else{
                    $query[$key] = $value;
                }                
            }
        }
        
        $query_result = [];
        foreach ($query as $k => $value) {
            $k = str_replace('amp;', '', $k);
            $query_result[$k] = $value;
        }

        if(!empty($remove)){
            foreach ($remove as $key) {
                unset($query_result[$key]);
            }
        }
        $query_result = http_build_query($query_result);         
        return !empty($query_result) ? $path . '?' . $query_result : $path;
    }

    public function getUrlCurrent() 
    {
        return Router::url(null, true);
    }

    public function getUrlPath() 
    {
        $request = $this->getView()->getRequest();
        return $request->scheme() . '://' . $request->host() . $request->getPath();
    }

    public function getUrlWebsite()
    {
        $request = $this->getView()->getRequest();
        return $request->scheme() . '://' . $request->host();
    }

    public function getParamsByKey($key = null)
    {
        if(empty($key)) return null;
        return $this->getView()->getRequest()->getQuery($key);    
    }

    public function replaceVariableSystem($str = null)
    {
        if(empty($str)) return '';
        
        $str = str_replace('{URL_TEMPLATE}', URL_TEMPLATE, $str);
        $str = str_replace('{CDN_URL}', CDN_URL, $str);
        $str = str_replace('{PATH_TEMPLATE}', PATH_TEMPLATE, $str);
        $str = str_replace('{CODE_TEMPLATE}', CODE_TEMPLATE, $str);
        $str = str_replace('{PAGE_URL}', PAGE_URL, $str);
        $str = str_replace('{LANGUAGE}', LANGUAGE, $str);

        return $str;
    }

    public function formatToList($data = [], $key_id = 'id', $key_value = 'name')
    {
        if(empty($data) || !is_array($data)) return [];

        $result = [];
        foreach ($data as $item) {
            $id = !empty($item[$key_id]) ? $item[$key_id] : null;
            $value = !empty($item[$key_value]) ? $item[$key_value] : null;

            if(is_null($id) || is_null($value)) continue;
            $result[$id] = $value;
        }

        return $result;
    }
}
