<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class BrandHelper extends Helper
{   
    public function getListBrands()
    {
    	$brands = TableRegistry::get('Brands')->getListBrands();
        return !empty($brands) ? $brands : [];
    }

    public function getBrands($params = []) 
    {
        $result = [];

        $params[FILTER][STATUS] = 1;
        $params[FIELD] = FULL_INFO;

        $brands = TableRegistry::get('Brands')->queryListBrands($params)->toList();

        if(!empty($brands)){
            foreach($brands as $k => $brand){
                $result[$k] = TableRegistry::get('Brands')->formatDataBrandDetail($brand);
            }
        }
        
        return $result;
    }
}