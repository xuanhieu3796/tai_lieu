<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Utility\Hash;

class CategoryHelper extends Helper
{
    public function getInfoCategory($id = null, $type = null, $params = [])
    {
        if(empty($id)) return [];
        if(empty($type) || !in_array($type, Configure::read('LIST_TYPE_CATEGORY'))) return [];

        $table = TableRegistry::get('Categories');
        $lang = !empty($params[LANG]) ? $params[LANG] : TableRegistry::get('Languages')->getDefaultLanguage();
    
        $category_product = $table->getDetailCategory($type, $id, $lang);

        if(empty($category_product)) return [];

        return $table->formatDataCategoryDetail($category_product);
    }

    public function countPostByCatId($id = null, $type = null) {

        if(empty($id)) return 0;
        if(empty($type) || !in_array($type, Configure::read('LIST_TYPE_CATEGORY'))) return [];

        $parent_category = TableRegistry::get('Categories')->getAllChildCategoryId($id);
        $result = 0;

        if($type == PRODUCT) {
        	$result = TableRegistry::get('Products')->find()->contain('CategoryProduct')->where([
        		'Products.deleted' => 0,
        		'Products.status' => ENABLE,
        		'CategoryProduct.category_id IN' => $parent_category
        	])->select('Products.id')->group('Products.id')->count();

            if(empty($result)) return 0;
        }
        
        if($type == ARTICLE) {
            $result = TableRegistry::get('Articles')->find()->contain('CategoryArticle')->where([
                'Articles.deleted' => 0,
                'Articles.status' => ENABLE,
                'CategoryArticle.article_id IN' => $parent_category
            ])->select('Articles.id')->group('Articles.id')->count();

            if(empty($result)) return 0;
        }

    	return $result;
    }

    private function parseDataCategoryDropdown($categories = [], $loop = 0)
    {
        $result = [];
        if(empty($categories)) return $result;

        $loop ++;
        $char = '- ';
        $char_level = '';

        for ($i = 1; $i < $loop; $i++) {
            $char_level .= $char;
        }

        foreach($categories as $category){           
            if(empty($category['id']) || empty($category['CategoriesContent']->name)) continue;
            $result[$category['id']] = $char_level . $category['CategoriesContent']->name;            
            if(!empty($category['children'])){
                $result += $this->parseDataCategoryDropdown($category['children'], $loop);    
            }
        }

        return $result;
    }

    public function getCategories($type = null, $lang = null, $params = []) 
    {
        $result = [];

        if(empty($type) || empty($lang) || !in_array($type, Configure::read('LIST_TYPE_CATEGORY'))){
            return $result;
        }

        $get_parent = !empty($params['get_parent']) ? true : false;

        $params['get_parent'] = $get_parent;
        $params[FILTER][STATUS] = 1;
        $params[FILTER][LANG] = $lang;
        $params[FILTER][TYPE] = $type;

        $categories = TableRegistry::get('Categories')->queryListCategories($params)->nest('id', 'parent_id')->toList();
        if(!empty($categories)){
            foreach($categories as $k => $category){
                $result[$k] = TableRegistry::get('Categories')->formatDataCategoryDetail($category, $lang);
            }
        }
        
        return $result;
    }

    public function getCategoriesForDropdown($type = null, $lang = null, $params = [])
    {
        $result = [];

        if(empty($type) || empty($lang) || !in_array($type, Configure::read('LIST_TYPE_CATEGORY'))){
            return $result;
        }

        $get_parent = !empty($params['get_parent']) ? true : false;

        $params['get_parent'] = $get_parent;
        $params[FIELD] = LIST_INFO;
        $params[FILTER][STATUS] = 1;
        $params[FILTER][LANG] = $lang;
        $params[FILTER][TYPE] = $type;

        $categories = TableRegistry::get('Categories')->queryListCategories($params)->nest('id', 'parent_id')->toList();

        if(!empty($categories)){
            $result = $this->parseDataCategoryDropdown($categories, 0);
        }

        return $result;
    }

    public function getListCategories($type = null, $lang = null, $params = []) 
    {

        $result = [];

        if(empty($type) || empty($lang) || !in_array($type, Configure::read('LIST_TYPE_CATEGORY'))){
            return $result;
        }

        $get_parent = !empty($params['get_parent']) ? true : false;

        $params['get_parent'] = $get_parent;
        $params[FILTER][STATUS] = 1;
        $params[FILTER][LANG] = $lang;
        $params[FILTER][TYPE] = $type;

        $categories = TableRegistry::get('Categories')->queryListCategories($params)->nest('id', 'parent_id')->toList();
        if(!empty($categories)){
            foreach($categories as $k => $category){
                $result[$k] = TableRegistry::get('Categories')->formatDataCategoryDetail($category, $lang);
            }
        }
        
        return $result;
    }
}
