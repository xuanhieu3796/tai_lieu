<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class ArticleHelper extends Helper
{
    public function getArticles($params = [], $lang = null) 
    {
        $result = [];

        $languages = TableRegistry::get('Languages')->getList();
        if(empty($lang) || empty($languages[$lang])) $lang = LANGUAGE;
        $params[FILTER][LANG] = $lang;

        if(!isset($params[FILTER][STATUS])) $params[FILTER][STATUS] = 1;

        $limit = !empty($params['limit']) ? intval($params['limit']) : 10;
        $articles = TableRegistry::get('Articles')->queryListArticles($params)->limit($limit)->toList();

        if(!empty($articles)){
            foreach($articles as $k => $article){
                $result[$k] = TableRegistry::get('Articles')->formatDataArticleDetail($article, $lang);
            }
        }
        
        return $result;
    }
}
