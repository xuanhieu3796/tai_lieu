<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class AttributeHelper extends Helper
{   

    public function getListOptions($attribute_code = null, $lang = null)
    {
        if(empty($attribute_code)) return [];

        if(empty($lang)){
            $lang = LANGUAGE;
        }        

        $attribute_info = TableRegistry::get('Attributes')->find()->where([
            'deleted' => 0,
            'code' => $attribute_code
        ])->select()->first();

        if(empty($attribute_info)) return [];        
        $attribute_id = !empty($attribute_info['id']) ? intval($attribute_info['id']) : null;

        $all_options = Hash::combine(TableRegistry::get('AttributesOptions')->getAll($lang), '{n}.id', '{n}.name', '{n}.attribute_id'); 
        $result = !empty($all_options[$attribute_id]) ? $all_options[$attribute_id] : [];
        
        return $result;
    }

}
