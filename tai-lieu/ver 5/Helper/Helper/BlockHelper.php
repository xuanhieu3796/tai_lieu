<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

class BlockHelper extends Helper
{

    public function getLocale($key = null, $data_extend = [])
    {
        if(empty($data_extend['locale'][LANGUAGE][$key])) return $key;
        return $data_extend['locale'][LANGUAGE][$key];
    }

    public function checkViewExist($block_type = null, $view = null)
    {
        $path_view = PATH_TEMPLATE . BLOCK . DS . $block_type . DS . $view;
        $file = new File($path_view, false);
        if(!$file->exists()){
            return false;
        }
        return true;
    }

}
