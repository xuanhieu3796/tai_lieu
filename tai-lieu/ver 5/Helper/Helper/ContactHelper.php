<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class ContactHelper extends Helper
{
    public function getDetailContact($contact_id = null, $param = [])
    {
        if(empty($contact_id)) return [];

        $table = TableRegistry::get('Contacts');

        $contact = $table->getDetailContact($contact_id, $param);
        
        return $table->formatDataContactDetail($contact);
    }
}
