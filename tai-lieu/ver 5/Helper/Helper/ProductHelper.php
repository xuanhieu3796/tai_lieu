<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class ProductHelper extends Helper
{
    public function getProducts($params = [], $lang = null) 
    {
        $result = [];

        $languages = TableRegistry::get('Languages')->getList();
        if(empty($lang) || empty($languages[$lang])) $lang = LANGUAGE;
        $params[FILTER][LANG] = $lang;

        if(!isset($params[FILTER][STATUS])) $params[FILTER][STATUS] = 1;
        if(!isset($params[FILTER][STATUS_ITEM])) $params[FILTER][STATUS_ITEM] = 1;
        
        $limit = !empty($params['limit']) ? intval($params['limit']) : 10;
        $products = TableRegistry::get('Products')->queryListProducts($params)->limit($limit)->toList();

        if(!empty($products)){
            foreach($products as $k => $product){
                $result[$k] = TableRegistry::get('Products')->formatDataProductDetail($product, $lang);
            }
        }
        
        return $result;
    }
}
