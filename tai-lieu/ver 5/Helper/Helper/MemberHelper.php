<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class MemberHelper extends Helper
{
    public function getMemberInfo()
    {
        $session = $this->getView()->getRequest()->getSession();
        return $session->read(MEMBER);     
    }

    public function getDetailCustomer($customer_id = null, $param = [])
    {
        if(empty($customer_id)) return [];

        $table = TableRegistry::get('Customers');

        $customer = $table->getDetailCustomer($customer_id, $param);
        
        return $table->formatDataCustomerDetail($customer);
    }
}
