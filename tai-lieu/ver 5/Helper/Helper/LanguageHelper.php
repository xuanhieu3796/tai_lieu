<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class LanguageHelper extends Helper
{   
    public function getList()
    {
        return TableRegistry::get('Languages')->getList();
    }
}
