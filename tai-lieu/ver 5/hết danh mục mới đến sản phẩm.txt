hết danh mục mới đến sản phâm

{strip}
{assign var = col value = ""}
{if !empty($data_extend['col'])}
    {assign var = col value = $data_extend['col']}
{/if}

{assign var = arr_category value = $this->Category->getListCategories(PRODUCT, LANGUAGE, [
    "{FILTER}" => [
        'parent_id' => PAGE_CATEGORY_ID
    ],
    "{SORT}" => [
        "{FIELD}" => 'position',
        "{SORT}" => "{ASC}"
    ]
])}

{if !empty($arr_category)}
    <div class="row">
        {foreach from = $arr_category item = cate}
            <div class="col-6 col-sm-6 col-md-4">
                <div class="product-item shadow overflow-hidden rounded-15 mb-20">
                    <div class="img_category position-relative rti-100 mb-10">
                        {if !empty($cate.image_avatar)}
                            {assign var = url_img value = "{CDN_URL}{$this->Utilities->checkInternalUrl($cate.image_avatar, 500)}"}
                        {else}
                            {assign var = url_img value = "data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="}
                        {/if}
                        <a class="" href="{if !empty($cate.url)}{$this->Utilities->checkInternalUrl($cate.url)}{else}/{/if}">
                            {$this->LazyLoad->renderImage([
                                'src' => $url_img, 
                                'alt' => $cate.name, 
                                'class' => 'img-fluid rti-abs-cover'
                            ])}
                        </a>
                    </div>
                    <div class="item-title-category pb-10 px-10 text-center">
                        <a class="font-weight-bold" href="{if !empty($cate.url)}{$this->Utilities->checkInternalUrl($cate.url)}{else}/{/if}">
                            {if !empty($cate.name)}
                                {$cate.name}
                            {/if}
                        </a>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
    {else}
    {if !empty($data_block.data)}
        <div class="row">
            {foreach from = $data_block.data item = product}
                {$this->element("../block/{$block_type}/item", [
                    'product' => $product, 
                    'col' => $col,
                    'is_slider' => false
                ])}
            {/foreach}
        </div>
    {else}
        {__d('template', 'khong_co_du_lieu')}
    {/if}
    {if !empty($block_config.has_pagination) && !empty($data_block[{PAGINATION}])}
        {$this->element('pagination', ['pagination' => $data_block[{PAGINATION}]])}
    {/if}

{/if}
{/strip}