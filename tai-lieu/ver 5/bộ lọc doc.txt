{assign var = current_url value = $this->Url->build()}
<div nh-filter-params class="mb-30">
	<h3 class="title-section-1">
		{__d('template', 'gia_san_pham')}
	</h3>
	<ul class="filter-section">
		<li>
			<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['price_to' => '5000000'], ['price_from'])}">
			    <span class="inner-switch circle">
					<span></span>
				</span>
				<span class="inner-name">
					{__d('template', 'duoi')} 5 {__d('template', 'trieu')}
				</span>
			</a>
		</li>
		
		<li>
			<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['price_from' => '5000000', 'price_to' => '8000000'])}">
			    <span class="inner-switch circle">
					<span></span>
				</span>
				<span class="inner-name">
					{__d('template', 'tu')} 5 {__d('template', 'trieu')} {__d('template', 'den')} 8 {__d('template', 'trieu')}
				</span>
			</a>
		</li>
		
		<li>
			<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['price_from' => '8000000', 'price_to' => '10000000'])}">
			    <span class="inner-switch circle">
					<span></span>
				</span>
				<span class="inner-name">
					{__d('template', 'tu')} 8 {__d('template', 'trieu')} {__d('template', 'den')} 10 {__d('template', 'trieu')}
				</span>
			</a>
		</li>
		
		<li>
			<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['price_from' => '10000000'], ['price_to'])}">
			    <span class="inner-switch circle">
					<span></span>
				</span>
				<span class="inner-name">
					{__d('template', 'tren')} 10 {__d('template', 'trieu')}
				</span>
			</a>
		</li>
		<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, [], ['price_from', 'price_to'])}" class="reset-attribute effect-border-scale ml-0">
		    {__d('template', 'xoa')}
		</a>
	</ul>
	

	
	<h3 class="title-section-1">
		{__d('template', 'trang_thai')}
	</h3>
	
	<div class="filter-section">
		<ul class="product-attribute-switch d-flex justify-content-start text-switch flex-wrap list-unstyled">
		    <li>
		        <a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['status' => 'featured'], [], ['merge' => true])}" class="inner-product-attribute">
    		        {__d('template', 'noi_bat')}
    		    </a>
		    </li>
		    
		    <li>
		        <a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['status' => 'discount'], [], ['merge' => true])}" class="inner-product-attribute">
    		        {__d('template', 'giam_gia')}
    		    </a>
		    </li>
		    
		    <li>
		        <a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, ['status' => 'stocking'], [], ['merge' => true])}" href="" class="inner-product-attribute">
    		        {__d('template', 'con_hang')}
    		    </a>
		    </li>
		</ul>
		<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, [], ['status'])}" class="reset-attribute effect-border-scale ml-0">
		    {__d('template', 'xoa')}
		</a>
	</div>
	

	

	<a href="javascript:;" nh-link-redirect="{$this->Utilities->addParamsToUrl($current_url, [], ['price_from', 'price_to', 'brand', 'status', 'item_color', 'item_size'])}" class="btn btn-submit">
	    {__d('template', 'dat_lai')}
	</a>
</div>


.title-section-1 {
  font-weight: bold;
    margin-bottom: 5px;
    color: var(--white);
    text-transform: uppercase;
    font-size: 15px;
    text-align: center;
    background: var(--color-main);
    display: block;
    padding: 5px; }
	

.filter-section {
    list-style: none;
    background: #fafafa;
    padding: 10px;
    margin-bottom: 20px;
  }

    .filter-section li .inner-switch > span {
      width: 15px;
      height: 15px;
      margin-right: 10px;
      display: block;
      position: relative;
      border: 1px solid #e3e3e3;
      -webkit-transition: all 0.2s ease-in-out;
      transition: all 0.2s ease-in-out; }
    .filter-section li .inner-switch.circle > span {
      border-radius: 50%;
      -webkit-border-radius: 50%;
      -moz-border-radius: 50%;
      -ms-border-radius: 50%;
      -o-border-radius: 50%; }
    .filter-section li > a {
      display: inline-flex;
      flex-direction: row;
      align-items: center;
      margin: 6px 0; }
      .filter-section li > a:hover {
        color: var(--color-main); }
        .filter-section li > a:hover .inner-switch > span {
          background-color: transparent !important; }
          .filter-section li > a:hover .inner-switch > span::after {
            content: "\ec32";
            font-family: "iconsax";
            font-weight: 900;
            position: absolute;
            top: 0px;
            left: 0;
            line-height: 1; }
      .filter-section li > a.active .inner-name {
        color: var(--color-main);
        }